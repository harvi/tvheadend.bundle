####################################################################################################
#
# Tvheadend.bundle
# Harvi Matharu
#
####################################################################################################

PREFIX = "/video/Tvheadend"
NAME = "Tvheadend Plugin"

# Imports
from operator import methodcaller
import base64, time, unicodedata

# Preferences
username = Prefs['tvh_usr'] 
password = Prefs['tvh_pwd']
hostname = Prefs['tvh_host']

# URL templates
tvUrlWithAuth = 'http://%s:%s@%s:9981/' % (username, password, hostname)
tvUrl = 'http://%s:9981/' % (hostname)
transcode = '?transcode=1&mux=mpegts&acodec=AAC&vcodec=H264&scodec=NONE&resolution=1080&bandwidth=0&language='

# This function is initially called by the PMS framework to initialize the plugin. This includes
# setting up the Plugin static instance along with the displayed artwork.
def Start():

# Setup the default breadcrumb title for the plugin
    ObjectContainer.title1 = NAME

# This main function will setup the displayed items.
# Initialize the plugin
@handler(PREFIX, NAME, thumb="icon-default.png", art="art-default.jpg")
def MainMenu():
    oc = ObjectContainer()
    oc.add(DirectoryObject(key=Callback(AllChannels), title="All Channels"))
    oc.add(PrefsObject(title="Settings"))
    return oc

@route(PREFIX + "/allchannels")
def AllChannels():
    base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
    headers = dict (Authorization="Basic %s" % base64string)
    jsonChannelData = JSON.ObjectFromURL(tvUrl + 'api/channel/list', headers = headers)
    jsonEpgData = JSON.ObjectFromURL(tvUrl + 'api/epg/grid?start=0&limit=1000', headers = headers)
    channelList = ObjectContainer(title1=NAME, title2="All Channels")

    for channelEntry in sorted(jsonChannelData['entries'], key=methodcaller('get', 'val', None)):
        channelName = channelEntry['val']
        channelUuid = channelEntry['key']
        title = channelName
        start = ''
        end = ''
        summary = ''
        duration = 0

        Log("Found channelEntry with channelName: %s and channelUuid: %s" % (channelName, channelUuid))

        for epgEntry in jsonEpgData['events']:
            if epgEntry['channelUuid'] == channelUuid:

                if 'title' in epgEntry.keys():
                    #tmpTitle = unicodedata.normalize('NFKD', epgEntry['title']).encode('ascii','ignore')
                    #title = '%s: %s' % (channelName, tmpTitle)
                    title = '%s: %s' % (channelName, epgEntry['title'])

                if 'start' in epgEntry.keys():
                    start = time.strftime("%H:%M", time.localtime(int(epgEntry['start'])))
                    title =  title + " " + start

                if 'stop' in epgEntry.keys():
                    end = time.strftime("%H:%M", time.localtime(int(epgEntry['stop'])))
                    title =  title + "-" + end

                if 'summary' in epgEntry.keys():
                    #tmpSummary = unicodedata.normalize('NFKD', epgEntry['summary']).encode('ascii','ignore')
                    #summary = '%s \n\n Starts:%s  Ends:%s' % (tmpSummary, start, end)
                    summary = '%s \n\n Starts:%s  Ends:%s' % (epgEntry['summary'], start, end)

                if 'duration' in epgEntry.keys():
                    duration = epgEntry['duration']*1000
                break

        Log("Creating VCO - Title:%s Summary: %s Duration: %s" % (title, summary, duration))
        vco = VideoClipObject(
            title = title,
            summary = summary,
            duration = duration,
            url='%sstream/channel/%s%s' % (tvUrlWithAuth, channelUuid, transcode))
        channelList.add(vco)

    return channelList
