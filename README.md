Tvheadend.bundle - Jan 2014

This project is a plugin for the plexmediaserver (plex.tv), and it works with the latest version of the Tvheadend backend (tvheadend.org).  You must compile Tvheadend from source and do not use the prebuilt packages.  Only when you build it yourself will you get the transcode option that you will need to enable if you want to see the content on your devices.

As far as devices are concerned, it seems to work fine from my Plex client on a Samsung TV and also via DLNA on my Sony TV.  It does not seem to work on a PlexWeb client (stream is being sent by tvheadend but not accepted by the web client) and I have not tested any other clients.

To install all you need to do is clone this repo into the "Plug-ins" directory of your plexmediaserver, restart and then you should see the plugin in the Video Channels section.  Configure the username/password and hostname for your Tvheadend server and then you should be good to go.

Disclaimer: Obviously I take absolutely no responsibility over any problems that you might incur both directly an indirectly from installing this software, and you do so at your own risk.  Also the plexmediaserver (plex.tv) an Tvheadend (tvheadend.org) names and software are controlled and owned by their respective organisations.

Having said that, enjoy!

Harvi.
